---
layout: post
theme: dark
---

{% for post in site.posts %}
  <div class="columns">
    <div class="column">
       <h2 class="is-size-3">
           <a class="post-title-link" href="{{ post.url }}">
               <i class="fas fa-link"></i> {{ post.title }}
          </a>
      </h2>
    </div>
  </div>
 {% endfor %}
