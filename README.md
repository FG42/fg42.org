# Build FG42 Website
In order to build this website you need to have ruby installed and follow the instructions:

```bash
$ bundle install
$ bundle exec jekyll build
```

for development:

```bash
$ bundle install
$ bundle exec jekyll serve -w
```
