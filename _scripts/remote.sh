#! /bin/bash

echo "[REMOTE]: Cleaning up..."
rm -rf ~/resources
echo "[REMOTE]: Extracting the package..."
tar zxf ~/tmp/package.tar.gz
echo "[REMOTE]: Installing the content..."
mkdir -p ~/fg42.org
cp -rv ~/_site/* ~/fg42.org/home/
chmod 755 ~/fg42.org/home/ -R
echo "[REMOTE]: Cleaning up..."
rm -rf ~/_site ~/tmp/
mkdir -p ~/tmp
echo "[REMOTE]: Done"
